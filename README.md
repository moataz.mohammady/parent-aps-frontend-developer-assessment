# MyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Login
You can user this account to loginto the system
 `Email => peter@klaven`
 `PassWord => cityslicka`
check `login.component.ts` for More details 
## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


-----------
[![N|Solid](https://s3.eu-west-2.amazonaws.com/parent-documents/assets/parent_logo.png)](http://parent.eu)

Dear Candidate,

Thank you for applying for the Frontend Developer position in Parent ApS.
Below you can find description of the coding assessment task.

Please send the assessment task as a **github repository** to <abdallah@parent.eu>.  

Angular verison: **6**.

# App Coding Task

The goal of this task is to check your knowledge, speed, code quality, UI background and testing techniques.

## Requirements

Create a simple web application that consumes a RESTful web service (<https://reqres.in/>) through the following endpoints:

- An API endpoint for logging in and authentication. (Login view)  :heavy_check_mark:
- An API endpoint for listing users. (Users list view)  :heavy_check_mark:
- An API endpoint for getting a single user. (Single user view)  :heavy_check_mark:
- An API endpoint for creating a user. (User creation view)  :heavy_check_mark:
- An API endpoint for deleting a user. (User removal modal)  :heavy_check_mark:
- An API endpoint for updating a user. (User updating view)  :heavy_check_mark:

### Acceptance Criteria:

- The web app should use a good user interface using bootstrap.
- The application should be unit tested and cover all the code functionalities.
- The application should display a success message to inform the user that the action was done successfully or failed.

### While building this web app:

- Use GIT and commit as often as possible using descriptive descriptions.
- Implement the task with Redux will be a super plus(Optional).
- Communicate with any questions needed.

### What we are looking for?

- Simple, clear, readable code, well structured.
- Solid principles.
- Application performance and usability.
- Does the application do what it promises? Can we find bugs or trivial flaws?
- Memory efficiency with large datasets.
- How well tested your application is? Can you give some metrics?

### Notes

- Implementations focusing on quality over feature completeness will be highly appreciated, donâ€™t feel compelled to implement everything and even if you are not able to complete the challenge, please do submit it anyways but at least the task should be working.

_Happy coding!_
