import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../service/auth.service';
import {logging} from 'selenium-webdriver';
import {MDBModalRef, MDBModalService} from 'angular-bootstrap-md';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('swalSucc') private swalSucc: SwalComponent;
  @ViewChild('swalErr') private swalErr: SwalComponent;
  modalRef: MDBModalRef;
  modalService: MDBModalService;
  loginForm: FormGroup;
  submitted: boolean = false;
  invalidLogin: boolean = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthenticationService) {
  }

  onSubmit() {
    console.log('####### mtz this.loginForm.invalid', this.loginForm.invalid);
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    if (this.loginForm.controls.email.value == 'peter@klaven' && this.loginForm.controls.password.value == 'cityslicka') {
      localStorage.removeItem('isAdmn');
      localStorage.setItem('isAdmin', 'true');
      this.router.navigate(['list-user']);
    } else {
      this.invalidLogin = true;
      this.swalErr.show();
      localStorage.setItem('isAdmin', 'fa;se');

    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

}
