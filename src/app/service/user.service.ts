import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User, UserCreate, UserCreatePost, UserUpdate, UserUpdatePost} from '../model/user.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Router} from '@angular/router';

@Injectable()
export class UserService {
  get baseUrl(): string {
    return this._baseUrl;
  }

  set baseUrl(value: string) {
    this._baseUrl = value;
  }

  constructor(private http: HttpClient, private router: Router,) {
  }

  // baseUrl: string = 'http://localhost:8080/user-portal/users';
  private _baseUrl = 'https://reqres.in/api/users';

  isAdmin(): any {
    if (localStorage.getItem('isAdmin') !== 'true') {
      return this.router.navigate(['login']);
    }
  }

  getUsers(): Observable<User[]> {
    return this.http.get<{ data: User[] }>(this._baseUrl + '?per_page=9', {observe: 'response'})
      .pipe(map(response => {
        return response.body.data;
      }));
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<{ data: User }>(this._baseUrl + '/' + id, {observe: 'response'})
      .pipe(map(response => {
        return response.body.data;
      }));
  }

  createUser(user): Observable<UserCreate> {
    return this.http.post<any>(this._baseUrl, user, {observe: 'response'})
      .pipe(map(response => {
        return response.body;
      }));
  }

  updateUser(user): Observable<UserUpdate> {
    return this.http.put<any>(this._baseUrl + '/' + user.id, user, {observe: 'response'})
      .pipe(map(response => {
        return response.body;
      }));
  }

  deleteUser(id: number) {
    /*
      * Request    /api/users/2
      * Response  204
    */
    return this.http.delete(this._baseUrl + '/' + id, {observe: 'response'})
      .pipe(map(response => {
        return response;
      }));
  }
}
