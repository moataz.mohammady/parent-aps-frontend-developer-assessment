import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';
import {User, UserCreate, UserCreatePost} from '../model/user.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  @ViewChild('swalSucc') private swalSucc: SwalComponent;
  @ViewChild('swalErr') private swalErr: SwalComponent;
  user: User;
  editForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.userService.isAdmin();
    let userId = localStorage.getItem('editUserId');
    if (!userId) {
      alert('Invalid action.');
      this.router.navigate(['list-user']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      avatar: ['', Validators.required]
    });
    this.userService.getUserById(+userId)
      .subscribe(response => {
        debugger;
        this.user = response;
        this.editForm.setValue(response);
      });
  }

  onSubmit() {
    this.userService.updateUser(this.editForm.value)
      .pipe(first())
      .subscribe(
        response => {
          if (response) {
            this.swalSucc.show();
            this.router.navigate(['list-user']);
          } else {
            this.swalErr.show();
          }
        },
        error => {
          alert(error);
        });
  }

}
