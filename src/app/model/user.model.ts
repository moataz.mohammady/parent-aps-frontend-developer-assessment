export class User {
  id: number;
  first_name: string;
  last_name: string;
  avatar: string;
}

export class UserUpdatePost {
  name: number;
  job: string;
}

export class UserUpdate {
  id: number;
  first_name: string;
  last_name: string;
  updatedAt: string;
}


export class UserCreate {
  id: number;
  job: string;
  name: string;
  createdAt: string;
}

export class UserCreatePost {
  id: number;
  job: string;
  name: string;
  createdAt: string;
}


