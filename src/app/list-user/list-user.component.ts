import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {User} from '../model/user.model';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  users: User[];
  @ViewChild('swalSucc') private swalSucc: SwalComponent;
  @ViewChild('swalErr') private swalErr: SwalComponent;
  @ViewChild('deleteSwal') private deleteSwal: SwalComponent;
  currentUser = {};

  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(response => {
        this.users = response;
      });
  }


  askDeleteUser(user: User): void {
    this.userService.isAdmin();
    this.currentUser = user;
    this.deleteSwal.show();
  }

  showUser(user: User): void {
    localStorage.removeItem('showUserId');
    localStorage.setItem('showUserId', user.id.toString());
    this.router.navigate(['show-user']);
  }


  deleteUser(user: User): void {
    this.userService.isAdmin();
    this.userService.deleteUser(user.id)
      .subscribe((response) => {
        if (response) {
          this.swalSucc.show();
          this.users = this.users.filter(u => u !== user);
        } else {
          this.swalErr.show();
        }
      }, () => {
      }, () => {
      })

    ;
  }

  editUser(user: User): void {
    this.userService.isAdmin();
    localStorage.removeItem('editUserId');
    localStorage.setItem('editUserId', user.id.toString());
    this.router.navigate(['edit-user']);
  }

  addUser(): void {
    this.userService.isAdmin();
    this.router.navigate(['add-user']);
  }
}
