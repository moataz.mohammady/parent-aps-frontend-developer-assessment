///<reference path="../service/user.service.ts"/>
import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';
import {User} from '../model/user.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {
  @ViewChild('swalSucc') private swalSucc: SwalComponent;
  @ViewChild('swalErr') private swalErr: SwalComponent;
  user: User;

  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    let userId = localStorage.getItem('showUserId');
    if (!userId) {
      this.router.navigate(['list-user']);
      return;
    }
    this.userService.getUserById(+userId)
      .subscribe(response => {
        this.user = response;
      });
  }

}
