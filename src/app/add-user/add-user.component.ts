import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../service/user.service';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  @ViewChild('swalSucc') private swalSucc: SwalComponent;
  @ViewChild('swalErr') private swalErr: SwalComponent;

  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService) {
  }


  addForm: FormGroup;

  ngOnInit() {
    this.userService.isAdmin();
    this.addForm = this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      job: ['', Validators.required],
    });

  }

  onSubmit() {
    this.userService.createUser(this.addForm.value)
      .subscribe(response => {
        if (response) {
          this.swalSucc.show();
          this.router.navigate(['list-user']);
        } else {
          this.swalErr.show();
        }
      });
  }

}
