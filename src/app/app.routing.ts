import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AddUserComponent} from './add-user/add-user.component';
import {ListUserComponent} from './list-user/list-user.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {ShowUserComponent} from './show-user/show-user.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'add-user', component: AddUserComponent},
  {path: 'list-user', component: ListUserComponent},
  {path: 'show-user', component: ShowUserComponent},
  {path: 'edit-user', component: EditUserComponent},
  {path: 'login', component: EditUserComponent},
  {path: '', component: LoginComponent}
];

export const routing = RouterModule.forRoot(routes);
